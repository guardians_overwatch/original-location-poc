# Setting up local environment to use kubernetes

---

### Prerequisites

#### Make sure you have VirtualBox installed (you should already have it installed)
```
$ brew cask install virtualbox
```

#### Make sure you have docker installed:
```
$ brew install docker
```

#### Install xhyve driver:
```
$ brew install docker-machine-driver-xhyve

# docker-machine-driver-xhyve need root owner and uid
$ sudo chown root:wheel $(brew --prefix)/opt/docker-machine-driver-xhyve/bin/docker-machine-driver-xhyve
$ sudo chmod u+s $(brew --prefix)/opt/docker-machine-driver-xhyve/bin/docker-machine-driver-xhyve
```

#### Install MiniKube:
```
$ curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.5.0/minikube-darwin-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/
```

#### Install Kubectl:
```
$ brew install kubectl
```

---

### Docker Related Things

#### Start minikube, docker-machine, and configure your shell
```
$ minikube start

$ eval "$(docker-machine env default)"
```

#### Pull the docker image down (I'll eventually add how to create an image, but for now an image already exists)
```
$ docker pull samdelamarter/locationpoc:0.0.2
```

---

### Kubernetes Related Things

#### Deploy and expose kubernetes pod
```
$ kubectl run location-poc --image=samdelamarter/locationpoc:0.0.2 --port=8080

$ kubectl expose deployment location-poc --type=NodePort
```

#### View your deployed pod
```
$ minikube dashboard
```

#### Scale your cluster to multiple pods
```
kubectl scale deployment location-poc --replicas=3
```

#### You can access your service at the address minikube set
```
$ minikube service location-poc --url

# You can get the full payload like so
$ curl $(minikube service location-poc --url)

# You can get an object via id like so
$ curl $(minikube service location-poc --url)/1
```

---

### Notes

* https://github.com/kubernetes/minikube
* https://github.com/kubernetes/minikube/releases
* http://kubernetes.io/docs/getting-started-guides/minikube/#download-kubectl
* http://kubernetes.io/docs/hellonode/
